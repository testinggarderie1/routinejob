const Datastore = require('@google-cloud/datastore');
const CustomDatastore = require('DataStore');

const datastore = new Datastore({
  projectId: 'manager-218900',
});

async function createPaymentSheet(month, year){
  if(!(await doesPaymentSheetExist(month, year))){
    await CustomDatastore.addEntity({kind : 'PaymentSheet', month: month, year: year});
    console.log(`registered new paymentSheet for ${month}-${year}`)
  }
}

async function createFamilyBill(paymentSheetId, familyId){
  if(!(await doesFamilyBilltExist(paymentSheetId, familyId))){
    await CustomDatastore.addEntity({kind : 'FamilyBill', familyId : familyId, paymentSheetId : paymentSheetId});
    console.log(`registered new FamilYBill ${familyId} for paymentSheet : ${paymentSheetId}`)
  }
}

async function createChildBill(paymentSheetId, familyBillId, childBillId, childId, amount){
  await CustomDatastore.addEntity({kind : 'ChildBill', paymentSheetId: paymentSheetId, childId: childId,
    familyBillId: familyBillId, amount: amount});

  console.log(`registered new ChildBill ${childBillId} for familyId : ${familyBillId}, paymentSheet: ${paymentSheetId} amount : ${amount}`)
}

async function doesFamilyBilltExist(paymentSheetId, familyId){
  const familyBill = getFamilyBillId(familyId);
  const entityKey = datastore.key(['PaymentSheet' , paymentSheetId, 'FamilyBill', familyBill]);
  const [entity] = await datastore.get(entityKey);
  if(entity){
    return true
  }
  return false;
}

async function doesPaymentSheetExist(month, year){
  const paymentSheetId = getPaymentSheetId(month, year);
  const entityKey = datastore.key(['PaymentSheet' , paymentSheetId]);
  const [entity] = await datastore.get(entityKey);
  if(entity){
    return true
  }
  return false;
}
async function getLastEndDateOfChilds(){
  const query = datastore.createQuery('Child');
  query.order('endDate');
  const [entities] = await datastore.runQuery(query);

  return entities[entities.length-1].endDate;
}

async function getFirstStartDateOfChilds(){
  const query = datastore.createQuery('Child');
  query.order('endDate');
  const [entities] = await datastore.runQuery(query);

  return entities[0].startDate;
}

function getFamilyIdOfChild(child){
  return child[datastore.KEY].parent.name;
}

function getChildIdOfChild(child){
  return child[datastore.KEY].name;
}

function getPaymentSheetId(month, year){
  return `PaymentSheet-${month}-${year}`;
}

function getFamilyBillId(familyId){
  return `FamilyBill-${familyId}`;
}

function getChildBillId(childId){
  return `ChildBill-${childId}`;
}

async function getChildrenPresentInPeriod(month, year){
  let childsInPeriod = [];
  const startPeriod = new Date(Date.UTC(year, month-1, 1));
  const endPeriod = new Date(Date.UTC(year, month, 0));
  const query = datastore.createQuery('Child');
  const [entities] = await datastore.runQuery(query);
  for (const child of entities) {
    if (child.startDate <= startPeriod && endPeriod <= child.endDate) {
      childsInPeriod.push(child);
    }

  }

  return childsInPeriod
}
module.exports = {getFirstStartDateOfChilds, getLastEndDateOfChilds, getChildrenPresentInPeriod,
  createPaymentSheet, getFamilyIdOfChild, createFamilyBill, createChildBill, getFamilyBillId,
  getPaymentSheetId, getChildBillId, getChildIdOfChild};