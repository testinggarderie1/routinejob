const Database = require('../DataBase');

describe('# Database', () => {
  it(' should get whole family by familyID', async () => {
    const result = await Database.getLastEndDateOfChilds();
    console.log(result);
  });

  it(' should get the childs inside the  period', async () => {
    const result = await Database.getChildrenPresentInPeriod(3, 2024);
    console.log(result);
  });
});