const DataBase = require('./DataBase');

async function makePaymentSheets(){
  const firstStartingPeriod = await DataBase.getFirstStartDateOfChilds();
  const lastEndingPeriod =  await DataBase.getLastEndDateOfChilds();

  const startingPeriod = new Date(firstStartingPeriod.getFullYear(), firstStartingPeriod.getMonth(), 1);
  const endingPeriod = new Date(lastEndingPeriod.getFullYear(), lastEndingPeriod.getMonth(), 1);
  const numberOfPeriods = monthDiff(startingPeriod, endingPeriod);

  for(let i =0; i <= numberOfPeriods; i++){
    const currentPeriod = new Date(startingPeriod.setMonth(startingPeriod.getMonth()+1));
    const currentMonth = currentPeriod.getMonth();
    const currentYear = currentPeriod.getFullYear();

    await DataBase.createPaymentSheet(currentMonth, currentYear);
    const childrensInPeriod = await DataBase.getChildrenPresentInPeriod(currentMonth, currentYear);

    const paymentSheetId = DataBase.getPaymentSheetId(currentMonth, currentYear);
    for(const child of childrensInPeriod){
      const familyId = DataBase.getFamilyIdOfChild(child);
      const familyBillId = DataBase.getFamilyBillId(familyId);
      await DataBase.createFamilyBill(paymentSheetId, familyId);

      const childId = DataBase.getChildIdOfChild(child);
      const childBillId = DataBase.getChildBillId(childId);
      const childCost = getChildCost(child.startDate, child.endDate, currentPeriod);
      await DataBase.createChildBill(paymentSheetId, familyBillId, childBillId, childId, childCost);

    }
  }

}

function getDaysSpentForPeriod(startDate, endDate, currentPeriod){
  const startOfPeriod = new Date(currentPeriod.getFullYear(), currentPeriod.getMonth(), 1);
  const endOfPeriod = new Date(currentPeriod.getFullYear(), currentPeriod.getMonth()+1, 0);
  const firstDay = startOfPeriod < startDate ? new Date(startDate) : new Date(startOfPeriod);
  const lastday = endOfPeriod > endDate ? new Date(endDate) : new Date(endOfPeriod);
  return datediff(firstDay, lastday);
}

function getChildCost(startDate, endDate, currentPeriod){
  const dayprice = 50;
  return getDaysSpentForPeriod(startDate, endDate, currentPeriod) * dayprice;
}

function datediff(first, second) {
  return Math.round((second-first)/(1000*60*60*24));
}

function monthDiff(d1, d2) {
  let months;
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= d1.getMonth() + 1;
  months += d2.getMonth();
  return months <= 0 ? 0 : months;
}



makePaymentSheets();